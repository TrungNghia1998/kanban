import React, { Component } from "react";
import "./App.css";

import { Route, Switch, Redirect } from "react-router-dom";

// import FormTask from "./components/FormTask/FormTask";
import UserForm from "./components/UserForm";
import ListTask from "./components/ListTask/ListTask";

// import Modal from "./components/Modal";
// import ModalSwap from './components/ModalSwap';
// import ModalEditUser from './components/ModalEditUser';

class App extends Component {
  state = {
    informationUser: null, // HoTen, Ngay, Email, Tuoi, Gioi Tinh
    tasks: [],
    showModalSwap: false,
    showModalEditUser: false,
    idTaskSwap: null,
    informationEditUser: null
  };

  registerHandler = user => {
    let inforUser = { ...this.state.informationUser };
    inforUser = user;
    inforUser.id = Math.random();
    this.setState({
      informationUser: inforUser,
    });
    localStorage.setItem("InformationUser", JSON.stringify(inforUser));
  };

  toggleModalSwap = (id) => {
    this.setState({
      showModalSwap: !this.state.showModalSwap,
      idTaskSwap: id
    });
  };

  toggleModalEditUser = () => {
    this.setState({
      showModalEditUser: !this.state.showModalEditUser
    });
    
  };

  AddTask = task => {
    let { tasks } = this.state;
    let newTask = {
      ...task,
      id: Math.random() + Math.random()
    }
    tasks.push(newTask);
    
    this.setState({
      tasks,
    });
    localStorage.setItem("List Tasks", JSON.stringify(tasks));
  };

  DeleteTask = id => {
    let { tasks } = this.state;
    let index = this.findIndex(id);
    if (index !== -1) {
      tasks.splice(index, 1);
      this.setState({
        tasks: tasks,
      });
      localStorage.setItem("List Tasks", JSON.stringify(tasks));
    }
  };

  onUpdateUser = (e, data) => {
    e.preventDefault();
    console.log(data);
    this.setState({
      showModalEditUser: !this.state.showModalEditUser,
      informationEditUser: data,
      informationUser: data
    });
    localStorage.setItem("InformationUser", JSON.stringify(data));
  }

  findIndex = id => {
    let { tasks } = this.state;
    let result = -1;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        result = index;
      }
    });
    return result;
  };

  UpdateTask = (id, task) => {
    let { tasks } = this.state;
    tasks[id].position = task.position;
    this.setState({
      tasks: tasks,
    });
    localStorage.setItem("List Tasks", JSON.stringify(tasks));
  };

  onUpdate = data => {
    console.log(data);
    let { tasks, idTaskSwap } = this.state;
    const position = parseInt(data.position);
    let index = this.findIndex(idTaskSwap);
    let task = { ...tasks[index] };
    task = {
      ...task,
      position: position,
    };
    tasks.splice(index, 1, task);
    localStorage.setItem("List Tasks", JSON.stringify(tasks));
    this.setState({
      tasks,
      showModalSwap: !this.state.showModalSwap,
    });
  };

  render() {
    let routes = (
      <Switch>
        <Route
          path="/"
          exact
          component={() => <UserForm onRegister={this.registerHandler} />}
        />
        <Route
          path="/task"
          component={() => (
            <ListTask
              onAddTask={this.AddTask}
              tasks={this.state.tasks}
              onDelete={this.DeleteTask}
              onUpdate={this.onUpdate}
              onUpdateUser = {this.onUpdateUser}
              showModalSwap={this.state.showModalSwap}
              showModalEditUser={this.state.showModalEditUser}
              toggleModalSwap={this.toggleModalSwap}
              informationEditUser={this.state.informationEditUser}
              toggleModalEditUser={this.toggleModalEditUser}
              user={localStorage.getItem("InformationUser")}
            />
          )}
        />
        <Redirect to="/" />
      </Switch>
    );

    return (
      <div className="App">
        {/* <UserForm link='/task' onSubmit = { this.registerHandler } />  */}
        {routes}
      </div>
    );
  }
}

export default App;
