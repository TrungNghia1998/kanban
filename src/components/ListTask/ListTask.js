import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Modal from "../Modal";
import ModalSwap from "../ModalSwap";
import ModalEditUser from "../ModalEditUser";

class UserForm extends Component {
  state = {
    name: "",
    date: "",
    email: "",
    age: 1,
    gender: false,
    toggleModal: false,
    toggleModalSwap: false,
    position: 1,
  };

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onAddTask(this.state); //Truyền state ra ngoài App.js
    this.onClearForm();
    this.props.history.push("/task");
  };

  onClearForm = () => {
    this.setState({
      name: "",
      date: "",
      email: "",
      age: 1,
      gender: false,
    });
  };

  toggleModal = () => {
    this.setState({
      toggleModal: !this.state.toggleModal,
    });
  };

  render() {
    let data = this.props.user;
    let dataJson = JSON.parse(data);

    let modal = null;
    if (this.state.toggleModal) {
      modal = (
        <Modal
          onSubmit={this.props.onAddTask}
          user={dataJson.name}
          toggle={this.toggleModal}
        />
      );
    } else {
      modal = null;
    }

    let modalSwap = null;
    if (this.props.showModalSwap) {
      modalSwap = (
        <ModalSwap
          onSubmit={this.props.onUpdate}
          toggle={() => this.props.toggleModalSwap(-1)}/>
      );
    } else {
      modalSwap = null;
    }

    let modalEditUser = null;
    console.log(this.props.showModalEditUser);
    if (this.props.showModalEditUser) {
      modalEditUser = (
        <ModalEditUser
          onSubmit={this.props.onUpdateUser}
          user={dataJson}
          toggle={this.props.toggleModalEditUser}
        />
      );
    } else {
      modalEditUser = null;
    }

    let { tasks } = this.props;
    let tasksNew = [];
    let tasksProcess = [];
    let tasksResolved = [];
    tasks.forEach(function(task) {
      if(task.position === 1) {
        tasksNew.push(task);
      }
      if(task.position === 2) {
        tasksProcess.push(task);
      }
      if(task.position === 3) {
        tasksResolved.push(task);
      }
    });
    let elementTasksNew = null;
    let elementTasksProcess = null;
    let elementTasksResolved = null;

    if (tasksNew) {
      elementTasksNew = tasksNew.map((taskNew, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskNew.name}
              <button
                onClick={() => this.props.toggleModalSwap(taskNew.id)}
                className="btn btn-warning"
              >
                Edit
              </button>
              <button
                onClick={() => this.props.onDelete(taskNew.id)}
                className="btn btn-danger"
              >
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksNew = null;
    }

    if (tasksProcess) {
      elementTasksProcess = tasksProcess.map((taskProcess, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskProcess.name}
              <button
                onClick={() => this.props.toggleModalSwap(taskProcess.id)}
                className="btn btn-warning"
              >
                Edit
              </button>
              <button
                onClick={() => this.props.onDelete(taskProcess.id)}
                className="btn btn-danger"
              >
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksProcess = null;
    }

    if (tasksResolved) {
      elementTasksResolved = tasksResolved.map((taskResolved, index) => {
        return (
          <div className="panel-body" key={index}>
            <div className="row">
              {taskResolved.name}
              <button
                onClick={() => this.props.toggleModalSwap(taskResolved.id)}
                className="btn btn-warning">
                Edit
              </button>
              <button
                onClick={() => this.props.onDelete(taskResolved.id)}
                className="btn btn-danger">
                Delete
              </button>
            </div>
          </div>
        );
      });
    } else {
      elementTasksProcess = null;
    }

    return (
      <div>
        {modal}
        {modalSwap}
        {modalEditUser}
        <div>
          <nav className="navbar navbar-inverse">
            <div className="container-fluid">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <a href="/" onClick={(event) => this.props.onUpdateUser(event, dataJson)}>
                     {dataJson.email}
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
        <div className="form-group">
          <div className="col-xs-12">
            <div className="col-xs-4">
              <div className="panel panel-primary">
                <div className="panel-heading">New</div>
                {elementTasksNew}
              </div>
            </div>

            <div className="col-xs-3">
              <div className="panel panel-primary">
                <div className="panel-heading">Process</div>
                {elementTasksProcess}
              </div>
            </div>

            <div className="col-xs-3">
              <div className="panel panel-primary">
                <div className="panel-heading">Resolved</div>
                {elementTasksResolved}
              </div>
            </div>

            <div className="col-xs-2">
              <button onClick={this.toggleModal} className="btn btn-success">
                Thêm mới
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(UserForm);
