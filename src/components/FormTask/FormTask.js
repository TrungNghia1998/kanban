import React, { Component } from "react";

class FormTask extends Component {
  state = {
    name: "",
    description: "",
  };

  onSubmit = event => {};

  openModal = () => {
    
  };

  render() {
    return (
      <div className="col-xs-6">
        <form onSubmit={this.onSubmit}>
          <legend>Tao task</legend>
          <div className="form-group">
            <label>Ten task</label>
            <input
              type="text"
              className="form-control"
              placeholder="Ten task"
              value="name"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Mo ta</label>
            <input
              type="text"
              className="form-control"
              placeholder="Mo ta"
              value="description"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Nguoi tao</label>
            <input
              type="text"
              className="form-control"
              readOnly
              value="user"
              placeholder="Nguoi tao"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default FormTask;
