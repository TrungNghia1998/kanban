import React, { Component } from "react";

class ModalEditUser extends Component {
  state = {
    name: this.props.user.name,
    date: this.props.user.date,
    email: this.props.user.email,
    age: this.props.user.age,
    gender: this.props.user.gender,
  };

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    let userInformation = { ...this.state };
    this.props.onUpdate(userInformation);
  };

  onCloseModal = () => {
    this.setState({
      closeModal: true,
    });
  };

  render() {
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.toggle}
                className="close"
                data-dismiss="modal">
                &times;
              </button>
              <h4 className="modal-title">Thong tin nguoi dung</h4>
            </div>
            <div className="modal-body">
              <form
                onSubmit={event => this.props.onSubmit(event, this.state)}>
                <div className="form-group">
                  <label>Ho ten</label>
                  <input
                    type="text"
                    name="name"
                    className="form-control"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Ngay sinh</label>
                  <input
                    type="date"
                    name="date"
                    value={this.state.date}
                    className="form-control"
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    value={this.state.email}
                    className="form-control"
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Tuoi</label>
                  <input
                    type="number"
                    name="age"
                    value={this.state.age}
                    className="form-control"
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Gioi tinh</label>
                  <select
                    className="form-control"
                    name="gender"
                    value={this.state.gender}
                    onChange={this.onChange}>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>

                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={this.props.toggle}
                className="btn btn-default"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

export default ModalEditUser;
