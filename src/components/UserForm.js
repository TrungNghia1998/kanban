import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class UserForm extends Component {
  state = {
    name: "",
    date: "",
    email: "",
    age: 1,
    gender: false,
  };

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onRegister(this.state); //Truyền state ra ngoài App.js
    this.onClearForm();
    this.props.history.push("/task");
  };

  onClearForm = () => {
    this.setState({
      name: "",
      date: "",
      email: "",
      age: 1,
      gender: false,
    });
  };

  render() {
    return (
      <div className="col-xs-6">
        <form onSubmit={this.onSubmit}>
          <legend>Thong tin nguoi dung</legend>

          <div className="form-group">
            <label>Ho ten</label>
            <input
              type="text"
              name="name"
              className="form-control"
              value={this.state.name}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Ngay sinh</label>
            <input
              type="date"
              name="date"
              value={this.state.date}
              className="form-control"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              name="email"
              value={this.state.email}
              className="form-control"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Tuoi</label>
            <input
              type="number"
              name="age"
              value={this.state.age}
              className="form-control"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Gioi tinh</label>
            <select
              className="form-control"
              name="gender"
              value={this.state.gender}
              onChange={this.onChange}>
              <option value="Male">Male</option>
              <option value="Female">Female</option>
            </select>
          </div>

          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default withRouter(UserForm);
