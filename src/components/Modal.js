import React, { Component } from "react";

class Modal extends Component {
  state = {
    name: "",
    description: "",
    position: 1
  };

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value,
    });
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.props.onSubmit(this.state);
  }

  onCloseModal = () => {
    this.setState({
      closeModal: true,
    });
  };

  render() {
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.toggle}
                className="close"
                data-dismiss="modal"
              >
                &times;
              </button>
              <h4 className="modal-title">Tao task</h4>
            </div>
            <div className="modal-body">
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <label>Ten task</label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Ten task"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Mo ta</label>
                  <input
                    type="text"
                    className="form-control"
                    name="description"
                    placeholder="Mo ta"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <label>Nguoi tao</label>
                  <input
                    type="text"
                    className="form-control"
                    readOnly
                    value={this.props.user}
                    placeholder="Nguoi tao"
                  />
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                onClick={this.props.toggle}
                className="btn btn-default"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

export default Modal;
