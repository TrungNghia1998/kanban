import React, { Component } from "react";

class ModalSwap extends Component {
  state = {
    position: 1,
  };

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value,
    });
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.state);
  };

  render() {
    let modal = (
      <div className="modal show" id="myModal" role="dialog">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                onClick={this.props.toggle}
                className="close"
                data-dismiss="modal"
              >
                &times;
              </button>
              <h4 className="modal-title">Chuyen vi tri</h4>
            </div>
            <div className="modal-body">
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <select
                    className="form-control"
                    name="position"
                    value={this.state.position}
                    onChange={this.onChange}
                  >
                    <option value="1">New</option>
                    <option value="2">In process</option>
                    <option value="3">Resolved</option>
                  </select>
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
                <button
                  type="button"
                  onClick={this.props.toggle}
                  className="btn btn-default">
                  Close
                </button>
              </form>
            </div>
            
          </div>
        </div>
      </div>
    );

    return <div>{modal}</div>;
  }
}

export default ModalSwap;
